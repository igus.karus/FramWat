Framwat
-
R/Shiny web application for water resources management

### Demo server

http://levis-framwat.sggw.pl:80/

### Installation

`install.R`

Script which installs all the necessary R dependencies

### Deployment

`redeploy.sh` 

Generic script which pull down the latest changes from git repository `origin/master` branch and starts `app.R` with
shiny server on port `8080`.

Be aware that script kills all R processes.

### Docker

`webii/framwat` - image with latest builds

installation

`docker pull webii/framwat`

running

`docker run -d -p 80:8080 webii/framwat:latest`

### Configuration

#### Translation

I18n is based on `Appsilon/shiny.i18n` (https://github.com/Appsilon/shiny.i18n/)

All languages and translation keys are listed in 
`config/translation.json`.

#### Indicators

`config/config.ymls`

Index `indicators`

Index `inputs`

#### Data input

### Testing

#### Unit tests

#### Functional tests

At first we need to have a selenium server which will provide a browser (in this case Chrome) to open an application
and go through functional tests.

`docker run -d -p 4444:4444 selenium/standalone-chrome:3.7.1-beryllium`

Running tests from command line

`test_dir("test")`
slopeRiverCalculation = function(SPU, DEM, iRiv) {
    iRiv$LENGiRiv <- SpatialLinesLengths(iRiv) ##Calculating length
    iRiv <- subset(iRiv, iRiv@data$LENGiRiv > xres(DEM)) ##Select river segments longer then Dem resolution
    iRiv@data$IDiRiv= (1:length(iRiv)) ##Create uniqest IDiRiv column for iRiver
    beginCluster(n=4) # run parallel processing for 4 core
    dfZ = raster::extract(DEM, iRiv, na.rm = TRUE, df=T) #extract  all terrein level along the river segment (one or more levels)
    stopifnot(all.equal(In_n_River_segment<-length(iRiv), Out_n_River_segment_DEM_Not_cover_River<-max(dfZ$ID)))  #DEM must cover river If not merge(join) give wrong relation

    #calculate statistic min, max and 90% quantile for each river segment
    names(dfZ)[1] <- "IDiRiv" # new name for first column
    names(dfZ)[2] <- "Z"
    dfZ$Counter = 1 # new column witch value 1 for counter statistic

    Zmin <- raster::aggregate(
        dfZ[,c("Z"), drop=FALSE],
        dfZ[,c("IDiRiv"), drop=FALSE],
        FUN="min", na.rm=TRUE
    )
    Zmax <- raster::aggregate(
        dfZ[,c("Z"), drop=FALSE],
        dfZ[,c("IDiRiv"), drop=FALSE],
        FUN="max", na.rm=TRUE
    )
    Zquant90 <- raster::aggregate(
        dfZ[,c("Z"), drop=FALSE],
        dfZ[,c("IDiRiv"), drop=FALSE],
        FUN= function(x, na.rm) {quantile(x, probs=0.9, type = 7, na.rm = TRUE)}, na.rm=TRUE
    )
    Zcount <- raster::aggregate(
        dfZ[,c("Counter"), drop=FALSE],
        dfZ[,c("IDiRiv"), drop=FALSE],
        FUN="sum", na.rm=TRUE
    )
    miRiv1<- merge(Zmin, Zmax, by = "IDiRiv", all.x=TRUE)
    miRiv2<- merge(miRiv1, Zquant90, by = "IDiRiv", all.x=TRUE)
    miRiv<- merge(miRiv2, Zcount, by = "IDiRiv", all.x=TRUE)

    names(miRiv)[2] <- "Zmin" # new name for second column
    names(miRiv)[3] <- "Zmax"
    names(miRiv)[4] <- "Zquant90"

    miRiv$Zmax <-ifelse (miRiv$Counter >= 10, miRiv$Zquant90, miRiv$Zmax) # change Zmax level to Zquant 90 for segment river which has more then 10 measures (because of usually river bank level are extracted too)
    miRiv <- merge(iRiv, miRiv, by = "IDiRiv", all.x=TRUE)


    #merging by ID
    miRiv$slop <- round(((miRiv@data$Zmax-miRiv@data$Zmin)/miRiv@data$LENGiRiv),3) # slop for one segment
    miRiv$slop <-replace (miRiv$slop, miRiv$slop<=0, 0.001)
    miRiv$slopLength <- miRiv$slop*miRiv$LENGiRiv
    #aggregating sum of Slop*Length and Length for calculating weighted average
    dfMiRiv <- data.frame(miRiv)
    AiRivSlopLength <- raster::aggregate(
    dfMiRiv[,c("slopLength"), drop=FALSE],
    dfMiRiv[,c("ID"), drop=FALSE],
    FUN="sum", na.rm=TRUE
    )
    AiRivLength <- raster::aggregate(
    dfMiRiv[,c("LENGiRiv"), drop=FALSE],
    dfMiRiv[,c("ID"), drop=FALSE],
    FUN="sum", na.rm=TRUE
    )

    AiRiv <- raster::merge(AiRivSlopLength, AiRivLength, by = "ID", all.x=TRUE)

    AiRiv$slopAvg <- AiRiv$slopLength/AiRiv$LENGiRiv*100
    slopAvg_out <- raster::merge(
    SPU,
    AiRiv,
    by='ID',
    all.x=TRUE
    ) # merging the data to SPU

    #replace Na,Nan & Inf to max TaySLO value = 0 slop
    slopAvgAvg <- mean(slopAvg_out$slopAvg, na.rm =T)
    slopAvg_out$slopAvg[is.na(slopAvg_out$slopAvg)] <- slopAvgAvg # replace "Na" to Average Slop
    slopAvg_out$slopAvg[is.nan(slopAvg_out$slopAvg)] <- slopAvgAvg # replace "NaN" to Average Slop
    slopAvg_out$slopAvg[is.infinite(slopAvg_out$slopAvg)] <- slopAvgAvg # replace "Inf" to Average Slop
    #checking if the SPU is equal to SubCatchment
    endCluster() # end parallel processing sesion
    return(list("values" = slopAvg_out$slopAvg, "shape" = miRiv))
}


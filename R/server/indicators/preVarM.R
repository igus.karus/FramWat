preVarMCalculation <- function(SPU, PrecVeg) {
    ras <- maskForNewRaster(SPU)

    PrecVeg <- dismo::voronoi(PrecVeg, ext=extent(SPU)) # point interpolation

    PrecVeg <- raster::intersect(SPU, PrecVeg)

    varMin_ras <- raster::rasterize(PrecVeg, ras, PrecVeg@data$pMinVeg)
    varAvg_ras <- raster::rasterize(PrecVeg, ras, PrecVeg@data$pAvgVeg)

    ratio_ras <- varMin_ras / varAvg_ras

    result <- raster::extract(ratio_ras, SPU, fun=mean, na.rm = TRUE)

    return(list("values" = result[,1], "raster" = ratio_ras))
}
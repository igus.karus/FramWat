
### CALCULATION METHOD  
$WaterYieldAvgFlow, [mm];$  

### DESCRIPTION  
Water yield (specific runoff) for mean flow [swMMQ]

### GROUP  
general and drought 


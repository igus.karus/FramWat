
### CALCULATION METHOD  
$bfi=  \frac{1}{NYears} * \frac {1}{12} * sum( \frac{swLQ\:ij}{swMQ\:ij}), i=1,NYears, j=1,12 months, [-];$   
where:  
swLQij-the lowest of daily flows witnin a month,   
swMQij-mean monthly flow;  

### DESCRIPTION  
Base Flow Index- groundwater contribution to river flow; Calculated for internal runoff from sub-basins.

### GROUP  
general, drought and flood 



### CALCULATION METHOD  
$FlowMinAvgRatio=  \frac {swMLQ}{ swMMQ}, [-];$   
where:  
swMLQ - mean low flow   
swMMQ - mean flow  

### DESCRIPTION   
Low flows assessment; Caculated for internal runoff from sub-basins.

### GROUP 
general and drought  


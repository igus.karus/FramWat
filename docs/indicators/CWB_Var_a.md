### CALCULATION METHOD  
$cwb\_Var\_a=  \frac{1}{NYears}*sum(\frac{cwbMax_i - cwbMin_i}{cwbAvg_i}), i=1,NYears [-];$  

### DESCRIPTION  
Climatic Water Balance  average  intra year variability- amplitude of monthly ($cwbMax~i~$ - $cwbMin~i~$) divided by mean monthly $cwb~i~$, averaged for the multiannual period (for the growing season).  

### GROUP  
general and drought


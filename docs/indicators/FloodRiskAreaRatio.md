### CALCULATION METHOD   
$FloodRiskAreaRatio= \frac{flood\: hazard\: zone\: area}{SPU\: area},  [-];$  

### DESCRIPTION  
FloodRiskArea is the ratio of flood hazard zone area to SPU area.

### GROUP  
general and flood   


### CALCULATION METHOD  

$FloVarRatio\_m=  \frac {swLMQ}{ swHMQ},[-];$  
where:  
swLMQ - low mean flow   
swHMQ - hight mean flow 

### DESCRIPTION  
Flow Variability multiannual: ratio of the lowest and the highest of yearly mean flows; Flow variability in dry and wet years in the multiannual period.

### GROUP   
general and drought  


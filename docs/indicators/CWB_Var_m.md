
### CALCULATION METHOD  
$cwb\_Var\_m=  \frac{cwbMinMth}{cwb}, i=1,NYears [-];$  

### DESCRIPTION   
Climatic Water Balance variability in the multiannual period- ratio of the lowest yearly (for the growing season) cwb value and multiannual cwb average.  

### GROUP   
general and drought

